
## Provectus Industrial Project
   
This project is Innopolis industrial project for Provectus company.
It parses websites of different airlines companies and compares scraped data with TripActions info.

### Request format
If you want to send request, you need send message to this url:
http://*/parse

JSON request:
```json
{
  "website": "united|southwest|lufthanza|airfrance|american_airlines",
  "from": "",
  "to": "",
  "departDate": "",
  "returnDate": "" 
}    
```
<b>website</b> - is website to get the flights information from.(for now only united airlines are supported)

<b>from</b> - code of departure airport.(PAR-Paris airport, VCE - Vermont airport).

<b>to</b> - code of destination airport.(PAR-Paris airport, VCE - Vermont airport).

<b>departDate</b> - date of departure in format('mmm dd, yyyy'); Example: Apr 13, 2020

<b>returnDate</b>(optional) - date of arrival in format('mmm dd, yyyy')

### Response format
Response formatted in the following way.

JSON response:
```json
{
  "website": "united|southwest|lufthanza|airfrance|american_airlines",
  "from": "",
  "to": "",
  "departDate": "",
  "returnDate": "",
  "flights": [
  {
    "departTime": "",
    "arrivalTime": "",
    "ticketNumber": "",
    "flightCode": "",
    "plane": ""
  }
] 
}    
```
<b>website</b> - is website to get the flights information from.(for now only united airlines are supported)

<b>from</b> - code of departure airport.(PAR-Paris airport, VCE - Vermont airport).

<b>to</b> - code of destination airport.(PAR-Paris airport, VCE - Vermont airport).

<b>departDate</b> - date of departure in format('mmm dd, yyyy')

<b>returnDate</b>(optional) - date of arrival in format('mmm dd, yyyy')

<b>flights</b> - list of flights on current date

<b>departTime</b> - time of departure in format('hh:mm')

<b>arrivalTime</b> - time of arrival in format('hh:mm')

<b>ticketNumber</b> - number of tickets which are still possible to buy

<b>flightCode</b> - code of the flight

<b>plane</b> - plane model
